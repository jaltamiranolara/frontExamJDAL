import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'

let apiUrl =  "https://mighty-refuge-81707.herokuapp.com/api/"

@Injectable()
export class AuthServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello AuthServiceProvider Provider');
  }
  
  post(payload, endpoint, xaccess){
    
    return new Promise ((resolve, reject) => {
      var auxHeader = {
        "Content-Type":  "application/json"
      }
      if(xaccess) auxHeader["x-access-token"] = localStorage.getItem('token')
      var headers = new HttpHeaders(auxHeader)
      this.http.post(apiUrl + endpoint, payload, {headers: headers})
            .subscribe(res => {
              resolve(res)
            },(error) => {
              reject(error)
            })
    })
    
  }

  get(endpoint){
    return new Promise((resolve, reject) => {
      console.log(localStorage.getItem('token'));
      var headers = new HttpHeaders({
        "Content-Type": "application/json",
        "x-access-token":  localStorage.getItem('token')
      })
      this.http.get(apiUrl + endpoint, {headers: headers})
            .subscribe(res => {
              resolve(res)
            },(error) => {
              reject(error)
            })
    })
  }

}
