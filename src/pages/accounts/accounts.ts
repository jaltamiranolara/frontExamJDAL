import { SigninPage } from './../signin/signin';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { RequestAccountPage } from './../request-account/request-account';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, App } from 'ionic-angular';
import jwt from 'jsonwebtoken'

@IonicPage()
@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html',
})
export class AccountsPage {
  accounts: Array<any>
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, public app: App, public authService: AuthServiceProvider) {
    var exp = jwt.decode(localStorage.getItem('token')).exp
    var currentTime = new Date().getTime() / 1000
    if(currentTime > exp){
      this.navCtrl.setRoot(SigninPage)
    }
  }

  async ionViewWillEnter(){
    await this.authService.get('accounts')
    .then((res) => {
      if(res) {
        this.accounts = res.response
      }
    }, (error)=>{
      console.log(error);
    })
  }

  signOut(){
    localStorage.removeItem('token')
    this.navCtrl.popToRoot()
  }

  onRequestAccount(){
    this.modalCtrl.create(RequestAccountPage).present()
  }
}
