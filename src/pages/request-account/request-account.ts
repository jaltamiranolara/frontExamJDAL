import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import jwt from 'jsonwebtoken'

@IonicPage()
@Component({
  selector: 'page-request-account',
  templateUrl: 'request-account.html',
})
export class RequestAccountPage {

  catalogs :any
  card : any
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, public authService: AuthServiceProvider) {
  }

  async ionViewWillEnter(){
    await this.authService.get('catalogs/cards')
    .then((res) => {
      if(res) {
        this.catalogs = res.response.type_cards
        console.log(this.catalogs);
      }
    }, (error)=>{
      this.alertCtrl.create({
        title: error.error.message,
        buttons: ['Ok']
      }).present();
    })
  }

  onDismiss(){
    this.viewCtrl.dismiss()
  }

  async requestCard(){
    await this.authService.post({
      userId: jwt.decode(localStorage.getItem('token')).id,
      type: this.card.type,
      name: this.card.name
    },'accounts',true)
    .then((res) => {
      if(res) {
        console.log(res);
      }
    }, (error)=>{
      console.log(error)
    })
  }
}
