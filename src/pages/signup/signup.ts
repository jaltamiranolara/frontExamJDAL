import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  constructor(public navCtrl: NavController, public authService: AuthServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signUp(value: {email:string, firstname: string, lastname: string, password: string}){
    this.authService.post(value, 'auth/user/create',false).then((res)=>{
      console.log(res)
    } , (error) => {
      console.log(error)
    })
  }
}
