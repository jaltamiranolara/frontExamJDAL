import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { SignupPage } from './../signup/signup';
import { AccountsPage } from './../accounts/accounts';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  response: any
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider, private alertCtrl: AlertController) {
    if(localStorage.getItem('token')){
      this.navCtrl.setRoot(AccountsPage)
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  async signIn(value : {email:string, password: string}){
    await this.authService.post(value, 'auth/user/authenticate',false)
    .then((res)=> {
      console.log(res)
      if(res){
        localStorage.setItem('token',res.token)
        this.navCtrl.push(AccountsPage)
      }
    }, (error) => {
      this.alertCtrl.create({
        title: error.error.success,
        subTitle: 'Intenta nuevamente',
        buttons: ['Ok']
      }).present();
    })
  }

  signUp(){
    this.navCtrl.push(SignupPage)
  }
}
